import React from "react";
import { FormControl, Select, MenuItem } from "@material-ui/core";

function Header({ country, countries,onCountryChange }) {
  return (
    <div className="app__header">
      <h2>Covid 19 Tracker</h2>
      <FormControl className="app__dropdown">
        <Select variant="outlined" value={country} onChange={onCountryChange}>
          <MenuItem value={country}>Worldwide</MenuItem>
          {countries.map((country, index) => (
            <MenuItem key={index} value={country.value}>
              {country.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}

export default Header;
