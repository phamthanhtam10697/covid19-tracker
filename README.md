# Covid 19 tracker project

## Demo
[Link](https://covid19-tracker-4c2c2.web.app/)

## Requirements
- Install Create-react-app

## Using
- react & react-hooks
- Material UI
- Chartjs-2
- React-leaflet

## Installation
Use the package manager npm to install dependencies.
> npm i

## Run
> npm start

## Deploy
- Firebase







